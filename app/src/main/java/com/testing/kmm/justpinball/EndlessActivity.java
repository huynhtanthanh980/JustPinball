package com.testing.kmm.justpinball;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class EndlessActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Handler handler = new Handler();
    Timer timer = new Timer();
    private ImageView shield;
    private ImageView red;
    private ConstraintLayout back;
    private LinearLayout shieldLayout;
    private TextView score, highScore, tryAgain;
    private int scoreCount = 0;
    private float ScreenWidth, ScreenHieght;
    private float X = 0, Y = 0;
    private double Xspeed = 0, Yspeed = 0;
    private boolean start = false, pause = false;
    private float shieldWidth = 0, ballWidth = 0;
    private float shieldX = 0, shieldY = 0;
    private int countSpeed = 0;
    private double heightRatio = 0, widthRatio = 0;
    private float x_cord = 0, x_start = 0;
    private int resume = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endless);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        back = findViewById(R.id.Back);
        back.setOnTouchListener(this);

        shield = findViewById(R.id.shield);

        shieldLayout = findViewById(R.id.shieldLayout);

        red = findViewById(R.id.red);

        ImageView pauseButton = findViewById(R.id.pause);
        pauseButton.setOnClickListener(this);

        score = findViewById(R.id.score);
        highScore = findViewById(R.id.highscore);
        tryAgain = findViewById(R.id.tryagain);

        shieldLayout.post(new Runnable() {
            @Override
            public void run() {
                shieldWidth = shield.getWidth();

                shieldY = shieldLayout.getY();
                x_cord = ScreenWidth / 2;
            }
        });

        back.post(new Runnable() {
            @Override
            public void run() {
                ScreenHieght = back.getHeight();
                heightRatio = ScreenHieght / 2960;

                ScreenWidth = back.getWidth();
                widthRatio = ScreenWidth / 1440;
            }
        });

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        changePos();
                    }
                });
            }
        }, 0, 10);
    }

    protected void startGame() {
        Xspeed = 0;
        Yspeed = (int) (-30 * heightRatio);

        ballWidth = red.getWidth();

        scoreCount = 0;
        score.setText(String.format(Locale.US, "%d", scoreCount));

        highScore.setVisibility(View.INVISIBLE);

        X = ScreenWidth / 2 - ballWidth / 2;
        Y = ScreenHieght / 2 - ballWidth / 2;
        red.setX(X);
        red.setY(Y);
        red.setVisibility(View.INVISIBLE);

        start = true;
        resume = 150;
    }

    protected void changePos() {
        if (!pause && resume >= 0 && start) {
            if (resume == 150) tryAgain.setText(R.string.ready);
            if (resume == 100) tryAgain.setText(R.string.set);
            if (resume == 50) tryAgain.setText(R.string.go);
            if (resume == 0) {
                tryAgain.setVisibility(View.INVISIBLE);
                red.setVisibility(View.VISIBLE);
            }
            resume--;
        }
        else if (start) {
            float Ypre = Y;
            X = X + (float) Xspeed;
            Y = Y + (float) Yspeed;

            if (X >= ScreenWidth - ballWidth) {
                X = ScreenWidth - ballWidth;
                Xspeed--;
                Xspeed = -Xspeed;
            }
            else if (X <= 0) {
                X = 0;
                Xspeed++;
                Xspeed = -Xspeed;
            }

            if (Ypre <= shieldY && Y >= shieldY && X <= shieldX + shieldWidth && X >= shieldX - ballWidth) {
                Y = shieldY;
                Yspeed = -Yspeed;
                scoreCount++;
                score.setText(String.format(Locale.US, "%d", scoreCount));
                changeSpeed();
            }
            else if (Y >= ScreenHieght) {
                tryAgain.setText(R.string.TapToTryAgain);
                tryAgain.setVisibility(View.VISIBLE);
                Xspeed = 0;
                Yspeed = 0;
                red.setVisibility(View.GONE);
                start = false;
            }
            else if (Y < 0) {
                Y = 0;
                Yspeed = -Yspeed;
                if (countSpeed == 2) {
                    countSpeed = 0;
                    Yspeed += widthRatio;
                }
                else countSpeed++;
            }

            red.setX(X);
            red.setY(Y);
        }
    }

    protected void changeSpeed() {
        if (X >= shieldX - red.getWidth() / 2 && X < shieldX)
            Xspeed -= (int) (2 * widthRatio);
        else if (X >= shieldX && X < shieldX + 1 * (shieldWidth / 6))
            Xspeed -= (int) (4 * widthRatio);
        else if (X >= shieldX + 1 * (shieldWidth / 6) && X < shieldX + 2 * (shieldWidth / 6))
            Xspeed -= (int) (6 * widthRatio);
        else if (X >= shieldX + 2 * (shieldWidth / 6) && X < shieldX + 3 * (shieldWidth / 6))
            Xspeed -= (int) (8 * widthRatio);
        else if (X > shieldX + 3 * (shieldWidth / 6) && X <= shieldX + 4 * (shieldWidth / 6))
            Xspeed += (int) (8 * widthRatio);
        else if (X > shieldX + 4 * (shieldWidth / 6) && X <= shieldX + 5 * (shieldWidth / 6))
            Xspeed += (int) (6 * widthRatio);
        else if (X > shieldX + 5 * (shieldWidth / 6) && X <= shieldX + shieldWidth)
            Xspeed += (int) (4 * widthRatio);
        else if (X > shieldX + shieldWidth && X > shieldX + shieldWidth + ballWidth / 2)
            Xspeed += (int) (2 * widthRatio);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (pause) {
                pause = false;
                start = true;
            }
            else if (!start) startGame();
            x_start = event.getX();
        }

        float x_cur = event.getX();

        x_cord += x_cur - x_start;

        if (x_cord >= ScreenWidth - shieldWidth / 2) {
            shieldX = ScreenWidth - shieldWidth;
            x_cord = ScreenWidth - shieldWidth / 2;
        }
        else if (x_cord <= shieldWidth / 2) {
            shieldX = 0;
            x_cord = shieldWidth / 2;
        }
        else {
            shieldX = x_cord - shieldWidth / 2;
        }

        shield.setX(shieldX);

        x_start = x_cur;

        return true;
    }

    @Override
    public void onClick(View v) {
        pause = true;
        tryAgain.setText(R.string.TapToResume);
        tryAgain.setVisibility(View.VISIBLE);
        resume = 150;
        start = false;
    }
}
