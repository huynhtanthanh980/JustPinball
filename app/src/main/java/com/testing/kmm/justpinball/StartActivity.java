package com.testing.kmm.justpinball;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    Handler handler = new Handler();
    Timer timer = new Timer();
    private ImageView red;
    private ConstraintLayout back;
    private float ScreenWidth;
    private float ScreenHieght;
    private float X = 0;
    private float Y = 0;
    private double Xspeed = 30;
    private double Yspeed = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        back = findViewById(R.id.Back);

        red = findViewById(R.id.red);

        Button buttonSolo = findViewById(R.id.buttonSolo);
        buttonSolo.setOnClickListener(this);

        Button buttonVersus = findViewById(R.id.buttonVersus);
        buttonVersus.setOnClickListener(this);

        Button buttonEndless = findViewById(R.id.buttonEndless);
        buttonEndless.setOnClickListener(this);

        back.post(new Runnable() {
            @Override
            public void run() {
                ScreenHieght = back.getHeight();
                ScreenWidth = back.getWidth();
            }
        });

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        changePos();
                    }
                });
            }
        }, 0, 10);
    }

    public void changePos() {
        X = X + (float) Xspeed;
        Y = Y + (float) Yspeed;

        if (X > ScreenWidth - red.getWidth()) {
            X = ScreenWidth - red.getWidth();
            Xspeed = -Xspeed;
        }
        else if (X < 0) {
            X = 0;
            Xspeed = -Xspeed;
        }
        else if (Y >= ScreenHieght - red.getHeight()) {
            Y = ScreenHieght - red.getHeight();
            Yspeed = -Yspeed;
        }
        else if (Y < 0) {
            Y = 0;
            Yspeed = -Yspeed;
        }

        red.setX(X);
        red.setY(Y);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSolo:
                Intent Defensive = new Intent(this, DefensiveActivity.class);
                startActivity(Defensive);
                break;
            case R.id.buttonVersus:
                Intent Versus = new Intent(this, VersusActivity.class);
                startActivity(Versus);
                break;
            case R.id.buttonEndless:
                Intent Endless = new Intent(this, EndlessActivity.class);
                startActivity(Endless);
                break;
        }
    }
}
