package com.testing.kmm.justpinball;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class DefensiveActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    Handler handler = new Handler();
    Timer timer = new Timer();
    private ImageView shield;
    private ImageView[] ball = new ImageView[10];
    private boolean[] ballIn = new boolean[10];
    private boolean[] ballOut = new boolean[10];
    private ConstraintLayout back;
    private LinearLayout shieldLayout;
    private TextView score, highScore, tryAgain;
    private int scoreCount = 0;
    private float ScreenWidth, ScreenHieght;
    private float[] X = new float[10], Y = new float[10];
    private double Yspeed = 0, Xspeed[] = new double[10];
    private boolean start = false, pause = false;
    private float shieldWidth = 0, ballWidth = 0;
    private float shieldX = 0, shieldY = 0;
    private double heightRatio = 0, widthRatio = 0;
    private float x_cord = 0, x_start = 0;
    private int resume = 0;
    private int countBall = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defensive);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        back = findViewById(R.id.Back);
        back.setOnTouchListener(this);

        shield = findViewById(R.id.shield);

        shieldLayout = findViewById(R.id.shieldLayout);

        ball[0] = findViewById(R.id.black);
        ball[1] = findViewById(R.id.blue);
        ball[2] = findViewById(R.id.brown);
        ball[3] = findViewById(R.id.darkblue);
        ball[4] = findViewById(R.id.green);
        ball[5] = findViewById(R.id.lightblue);
        ball[6] = findViewById(R.id.purple);
        ball[7] = findViewById(R.id.red);
        ball[8] = findViewById(R.id.verygreen);
        ball[9] = findViewById(R.id.yellow);

        for (int i = 0; i < 10; i++) ballIn[i] = false;

        ImageView pauseButton = findViewById(R.id.pause);
        pauseButton.setOnClickListener(this);

        score = findViewById(R.id.score);
        highScore = findViewById(R.id.highscore);
        tryAgain = findViewById(R.id.tryagain);

        shieldLayout.post(new Runnable() {
            @Override
            public void run() {
                shieldWidth = shield.getWidth();

                shieldY = shieldLayout.getY();
                x_cord = ScreenWidth / 2;
            }
        });

        back.post(new Runnable() {
            @Override
            public void run() {
                ScreenHieght = back.getHeight();
                heightRatio = ScreenHieght / 2960;

                ScreenWidth = back.getWidth();
                widthRatio = ScreenWidth / 1440;
            }
        });

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        changePos();
                    }
                });
            }
        }, 0, 10);
    }

    protected void startGame() {
        Yspeed = (float) (30 * heightRatio);

        ballWidth = ball[0].getWidth();

        countBall = 100;

        scoreCount = 0;
        score.setText(String.format(Locale.US, "%d", scoreCount));

        highScore.setVisibility(View.INVISIBLE);

        start = true;
        resume = 150;
    }

    protected void changePos() {
        if (!pause && resume >= 0 && start) {
            if (resume == 150) tryAgain.setText(R.string.ready);
            if (resume == 100) tryAgain.setText(R.string.set);
            if (resume == 50) tryAgain.setText(R.string.go);
            if (resume == 0) {
                tryAgain.setVisibility(View.INVISIBLE);
            }
            resume--;
        }
        else if (start) {
            countBall--;
            if (countBall == 0) {
                countBall = 100 - scoreCount;
                for (int i = 0; i < 10; i++) {
                    if (!ballIn[i]) {
                        ballIn[i] = true;
                        ballOut[i] = false;
                        Random random = new Random();
                        float X_cord = random.nextFloat() * (ScreenWidth - ballWidth);
                        float Y_cord = - 1000;
                        ball[i].setX(X_cord);
                        ball[i].setY(Y_cord);
                        Xspeed[i] = -(scoreCount + 1) + random.nextInt() * 2 * (scoreCount + 1);
                        ball[i].setVisibility(View.VISIBLE);
                        Log.i("KMM", "Ball " + i + ": " + X[i] + " | " + Y[i]);
                        break;
                    }
                }
            }
            for (int i = 0; i < 10; i++) {
                if (ballIn[i]) ballCheck(i);
            }
        }
    }

    protected void ballCheck(int i) {
        float Ypre = Y[i];
        X[i] = X[i] + (float) Xspeed[i];
        Y[i] = Y[i] + (float) Yspeed;

        if (X[i] >= ScreenWidth - ballWidth) {
            if (ballOut[i]) {
                X[i] = -ballWidth;
                ballIn[i] = false;
            }
            else {
                X[i] = 0;
                Xspeed[i] = -Xspeed[i];
            }
        }
        else if (X[i]<= 0) {
            if (ballOut[i]) {
                X[i] = -ballWidth;
                ballIn[i] = false;
            }
            else {
                X[i] = ScreenWidth - ballWidth;
                Xspeed[i] = -Xspeed[i];
            }
        }

        if (Ypre <= shieldY && Y[i] >= shieldY && X[i] <= shieldX + shieldWidth && X[i] >= shieldX - ballWidth) {
            Y[i] = shieldY;
            Yspeed = -Yspeed;
            scoreCount++;
            score.setText(String.format(Locale.US, "%d", scoreCount));
            ballOut[i] = true;
            changeSpeed(i);
        }
        else if (Y[i] >= ScreenHieght) {
            tryAgain.setText(R.string.TapToTryAgain);
            tryAgain.setVisibility(View.VISIBLE);
            for (int j = 0; j < 10; j++) {
                Xspeed[j] = 0;
                Yspeed = 0;
                ball[j].setVisibility(View.INVISIBLE);
            }
            start = false;
        }

        ball[i].setX(X[i]);
        ball[i].setY(Y[i]);
    }

    protected void changeSpeed(int i) {
        if (X[i] >= shieldX - ballWidth / 2 && X[i] < shieldX)
            Xspeed[i] = (int) (30 * widthRatio);
        else if (X[i] >= shieldX && X[i] < shieldX + 1 * (shieldWidth / 6))
            Xspeed[i] = (int) (20 * widthRatio);
        else if (X[i] >= shieldX + 1 * (shieldWidth / 6) && X[i] < shieldX + 2 * (shieldWidth / 6))
            Xspeed[i] = (int) (15 * widthRatio);
        else if (X[i] >= shieldX + 2 * (shieldWidth / 6) && X[i] < shieldX + 3 * (shieldWidth / 6))
            Xspeed[i] = (int) (10 * widthRatio);
        else if (X[i] > shieldX + 3 * (shieldWidth / 6) && X[i] <= shieldX + 4 * (shieldWidth / 6))
            Xspeed[i] = (int) (10 * widthRatio);
        else if (X[i] > shieldX + 4 * (shieldWidth / 6) && X[i] <= shieldX + 5 * (shieldWidth / 6))
            Xspeed[i] = (int) (15 * widthRatio);
        else if (X[i] > shieldX + 5 * (shieldWidth / 6) && X[i] <= shieldX + shieldWidth)
            Xspeed[i] = (int) (20 * widthRatio);
        else if (X[i] > shieldX + shieldWidth && X[i] > shieldX + shieldWidth + ballWidth / 2)
            Xspeed[i] = (int) (30 * widthRatio);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (pause) {
                pause = false;
                start = true;
            }
            else if (!start) startGame();
            x_start = event.getX();
        }

        float x_cur = event.getX();

        x_cord += x_cur - x_start;

        if (x_cord >= ScreenWidth - shieldWidth / 2) {
            shieldX = ScreenWidth - shieldWidth;
            x_cord = ScreenWidth - shieldWidth / 2;
        }
        else if (x_cord <= shieldWidth / 2) {
            shieldX = 0;
            x_cord = shieldWidth / 2;
        }
        else {
            shieldX = x_cord - shieldWidth / 2;
        }

        shield.setX(shieldX);

        x_start = x_cur;

        return true;
    }

    @Override
    public void onClick(View v) {
        pause = true;
        tryAgain.setText(R.string.TapToResume);
        tryAgain.setVisibility(View.VISIBLE);
        resume = 150;
        start = false;
    }
}
