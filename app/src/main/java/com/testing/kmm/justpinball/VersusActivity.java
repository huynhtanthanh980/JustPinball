package com.testing.kmm.justpinball;

import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class VersusActivity extends AppCompatActivity implements View.OnClickListener {

    Handler handler = new Handler();
    Timer timer = new Timer();
    private ImageView shield1;
    private ImageView shield2;
    private ImageView red;
    private ConstraintLayout back;
    private LinearLayout shieldLayout1, shieldLayout2;
    private TextView score1, score2, player1, player2;
    private int scoreCount1 = 0, scoreCount2 = 0;
    private float ScreenWidth, ScreenHieght;
    private float X = 0, Y = 0;
    private double Xspeed = 0, Yspeed = 0;
    private boolean start = false, start1 = false, start2 = false, pause = false, win = false;
    private float shieldWidth = 0, ballWidth = 0;
    private float shieldX[] = new float[2];
    private float shieldY[] = new float[2];
    private double heightRatio = 0, widthRatio = 0;
    private boolean touch1 = false, touch2 = false;
    private int touch1ID = 0, touch2ID = 0;
    private float X_cord[] = new float[2];
    private SparseArray<PointF> mPoint = new SparseArray<>();
    private int resume = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_versus);

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        back = findViewById(R.id.Back);

        score1 = findViewById(R.id.score1);
        score2 = findViewById(R.id.score2);

        player1 = findViewById(R.id.player1);
        player2 = findViewById(R.id.player2);

        ImageView pauseButton = findViewById(R.id.pause);
        pauseButton.setOnClickListener(this);

        shield1 = findViewById(R.id.shield1);
        shieldLayout1 = findViewById(R.id.shieldLayout1);

        shield2 = findViewById(R.id.shield2);
        shieldLayout2 = findViewById(R.id.shieldLayout2);

        red = findViewById(R.id.red);



        back.post(new Runnable() {
            @Override
            public void run() {
                shieldY[0] = shieldLayout1.getY();
                shieldY[1] = shieldLayout2.getY() + shield2.getHeight();

                ScreenHieght = back.getHeight();
                heightRatio = ScreenHieght / 2960;

                ScreenWidth = back.getWidth();
                widthRatio = ScreenWidth / 1440;

                shieldWidth = shield1.getWidth();

                X_cord[0] = ScreenWidth / 2 + shieldWidth / 2;
                X_cord[1] = ScreenWidth / 2 + shieldWidth / 2;

                ballWidth = red.getWidth();
                System.out.println(ballWidth);
            }
        });

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        changePos();
                    }
                });
            }
        }, 0, 10);
    }

    protected void startGame() {
        if (start1) Yspeed = (int) (-20 * heightRatio);
        else if (start2) Yspeed = (int) (20 * heightRatio);

        start1 = false;
        start2 = false;

        X = ScreenWidth / 2 - ballWidth / 2;
        Y = ScreenHieght / 2 - ballWidth / 2;
        red.setX(X);
        red.setY(Y);
        start = true;

        player1.setVisibility(View.VISIBLE);
        player2.setVisibility(View.VISIBLE);

        resume = 150;
    }

    protected void changePos() {
        if (!pause && resume >= 0 && start) {
            if (resume == 150) {
                player1.setText(R.string.ready);
                player2.setText(R.string.ready);
            }
            if (resume == 100) {
                player1.setText(R.string.set);
                player2.setText(R.string.set);
            }
            if (resume == 50) {
                player1.setText(R.string.go);
                player2.setText(R.string.go);
            }
            if (resume == 0) {
                player1.setVisibility(View.INVISIBLE);
                player2.setVisibility(View.INVISIBLE);
                red.setVisibility(View.VISIBLE);
            }
            resume--;
        }
        else if (start) {
            float Ypre = Y;
            X = X + (float) Xspeed;
            Y = Y + (float) Yspeed;

            if (X >= ScreenWidth - red.getWidth()) {
                X = ScreenWidth - ballWidth;
                Xspeed++;
                Xspeed = -Xspeed;
            }
            else if (X <= 0) {
                X = 0;
                Xspeed--;
                Xspeed = -Xspeed;
            }
            if (Ypre <= shieldY[0] && Y >= shieldY[0] && X <= shieldX[0] + shieldWidth + ballWidth / 2 && X >= shieldX[0] - ballWidth / 2) {
                Y = shieldY[0];
                Yspeed = -Yspeed;
                changeSpeed(0);
            }
            else if (Ypre >= shieldY[1] && Y <= shieldY[1] && X <= shieldX[1] + shieldWidth + ballWidth / 2 && X >= shieldX[1] - ballWidth / 2) {
                Y = shieldY[1];
                Yspeed = -Yspeed;
                changeSpeed(1);
            }
            else if (Y >= ScreenHieght) {
                Xspeed = 0;
                Yspeed = 0;
                red.setVisibility(View.GONE);
                start = false;
                win = true;
                scoreCount2++;
                score2.setText(String.format(Locale.US, "%d", scoreCount2));
                player1.setText(R.string.Player2Win);
                player2.setText(R.string.Player2Win);
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
            }
            else if (Y < 0) {
                Xspeed = 0;
                Yspeed = 0;
                red.setVisibility(View.GONE);
                start = false;
                win = true;
                scoreCount1++;
                score1.setText(String.format(Locale.US, "%d", scoreCount1));
                player1.setText(R.string.Player1Win);
                player2.setText(R.string.Player1Win);
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
            }

            red.setX(X);
            red.setY(Y);
        }
    }

    protected void changeSpeed(int i) {
        if (X == shieldX[i] +  shieldWidth / 2) {
            Yspeed = (i == 0) ? (int) (-40 * heightRatio) : (int) (40 * heightRatio);
            Xspeed = 0;
        }
        else {
            if (X >= shieldX[i] - ballWidth / 2 && X < shieldX[i])
                Xspeed = (int) (-30 * widthRatio);
            else if (X >= shieldX[i] && X < shieldX[i] + 1 * (shieldWidth / 6))
                Xspeed = (int) (-20 * widthRatio);
            else if (X >= shieldX[i] + 1 * (shieldWidth / 6) && X < shieldX[i] + 2 * (shieldWidth / 6))
                Xspeed = (int) (-10 * widthRatio);
            else if (X >= shieldX[i] + 2 * (shieldWidth / 6) && X < shieldX[i] + 3 * (shieldWidth / 6))
                Xspeed = (int) (-5 * widthRatio);
            else if (X > shieldX[i] + 3 * (shieldWidth / 6) && X <= shieldX[i] + 4 * (shieldWidth / 6))
                Xspeed = (int) (5 * widthRatio);
            else if (X > shieldX[i] + 4 * (shieldWidth / 6) && X <= shieldX[i] + 5 * (shieldWidth / 6))
                Xspeed = (int) (10 * widthRatio);
            else if (X > shieldX[i] + 5 * (shieldWidth / 6) && X <= shieldX[i] + shieldWidth)
                Xspeed = (int) (20 * widthRatio);
            else if (X > shieldX[i] + shieldWidth && X > shieldX[i] + shieldWidth + ballWidth / 2)
                Xspeed = (int) (30 * widthRatio);
            Yspeed = (i == 0) ? (int) (-20 * heightRatio) : (int) (20 * heightRatio);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int pointIndex = event.getActionIndex();
        int pointId = event.getPointerId(pointIndex);
        int maskAction = event.getActionMasked();

        switch (maskAction) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN: {

                float X_pre = event.getX(pointIndex);
                float Y_pre = event.getY(pointIndex);

                if (win) {
                    win = false;
                    player1.setText(R.string.HoldToReady);
                    player2.setText(R.string.HoldToReady);
                }

                if (Y_pre < ScreenHieght / 2 && !touch2) {
                    touch2 = true;
                    if (pause || !start) player2.setVisibility(View.INVISIBLE);
                    touch2ID = pointId;
                    if (pause && touch1) {
                        pause = false;
                        player1.setVisibility(View.VISIBLE);
                        player2.setVisibility(View.VISIBLE);
                        start = true;
                    }
                    else if (touch1 && !start) {
                        start = true;
                        start1 = true;
                        startGame();
                    }
                } else if (Y_pre > ScreenHieght / 2 && !touch1) {
                    touch1 = true;
                    if (pause || !start) player1.setVisibility(View.INVISIBLE);
                    touch1ID = pointId;
                    if (pause && touch2) {
                        pause = false;
                        player2.setVisibility(View.VISIBLE);
                        player1.setVisibility(View.VISIBLE);
                        start = true;
                    }
                    else if (touch2 && !start) {
                        start = true;
                        start2 = true;
                        startGame();
                    }
                }

                PointF fpoint = new PointF(X_pre, Y_pre);
                mPoint.put(pointId, fpoint);
            }

            case MotionEvent.ACTION_MOVE: {

                int pointCount = event.getPointerCount();
                int index = 2;

                for (int i = 0; i < pointCount; i++) {
                    PointF f = mPoint.get(event.getPointerId(i));
                    if (f != null) {
                        if (event.getPointerId(i) == touch1ID && touch1 && event.getY(i) > ScreenHieght / 2) index = 0;
                        else if (event.getPointerId(i) == touch2ID && touch2 && event.getY(i) < ScreenHieght / 2) index = 1;

                        if (index != 2) {
                            float x_cur = event.getX(i);

                            X_cord[index] += x_cur - f.x;

                            if (X_cord[index] >= ScreenWidth - shieldWidth / 2) {
                                shieldX[index] = ScreenWidth - shieldWidth;
                                X_cord[index] = ScreenWidth - shieldWidth / 2;
                            } else if (X_cord[index] <= shieldWidth / 2) {
                                shieldX[index] = 0;
                                X_cord[index] = shieldWidth / 2;
                            } else {
                                shieldX[index] = X_cord[index] - shieldWidth / 2;
                            }

                            if (index == 0) shield1.setX(shieldX[0]);
                            else shield2.setX(shieldX[1]);

                            f.x = x_cur;
                        }
                    }
                }
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP: {

                if (pointId == touch1ID && touch1) {
                    if (!start) {
                        player1.setVisibility(View.VISIBLE);
                    }
                    touch1ID = 0;
                    touch1 = false;
                } else if (pointId == touch2ID && touch2) {
                    if (!start) player2.setVisibility(View.VISIBLE);
                    touch2ID = 0;
                    touch2 = false;
                }
                mPoint.removeAt(pointId);
                break;
            }
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void onClick(View v) {
        if (start) {
            pause = true;
            player1.setText(R.string.HoldToResume);
            player2.setText(R.string.HoldToResume);
            player1.setVisibility(View.VISIBLE);
            player2.setVisibility(View.VISIBLE);
            resume = 150;
            start = false;
        }
    }
}